# Helper methods defined here can be accessed in any controller or view in the application

module Portal
  class App
    module SyncHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers SyncHelper
  end
end
